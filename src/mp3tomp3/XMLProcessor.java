package mp3tomp3;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XMLProcessor {
	private Document document;
	private DocumentBuilder builder;

	public XMLProcessor(String filename) throws SAXException, IOException, ParserConfigurationException{
		DocumentBuilderFactory dbfactory = DocumentBuilderFactory.newInstance();
		builder = dbfactory.newDocumentBuilder();
		File file = new File(filename);		
		document = builder.parse(file);
	}
	
	public String getElementByID(String elementName,int position){
		NodeList positions = document.getElementsByTagName(elementName);
		Element element = (Element)positions.item(position);
		
		return element.getTextContent();
	}


}

package mp3tomp3;

import it.sauronsoftware.jave.AudioAttributes;
import it.sauronsoftware.jave.Encoder;
import it.sauronsoftware.jave.EncoderException;
import it.sauronsoftware.jave.EncodingAttributes;
import it.sauronsoftware.jave.InputFormatException;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.CannotReadException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.audio.exceptions.InvalidAudioFrameException;
import org.jaudiotagger.audio.exceptions.ReadOnlyFileException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.xml.sax.SAXException;

public class MainClass {

	/**
	 * @param args
	 * @throws InvalidAudioFrameException 
	 * @throws ReadOnlyFileException 
	 * @throws TagException 
	 * @throws IOException 
	 * @throws CannotReadException 
	 * @throws EncoderException 
	 * @throws InputFormatException 
	 * @throws IllegalArgumentException 
	 * @throws ParserConfigurationException 
	 * @throws SAXException 
	 */
	public static void main(String[] args) throws CannotReadException, IOException, TagException, ReadOnlyFileException,
		InvalidAudioFrameException, IllegalArgumentException, InputFormatException, EncoderException, SAXException, ParserConfigurationException {
		// TODO Auto-generated method stub
		//long currentTime =System.currentTimeMillis();
		XMLProcessor settings = new XMLProcessor("settingDetails.xml");
		String srcDirectory =(settings.getElementByID("source_folder", 0)).trim();
		String destDirectory = (settings.getElementByID("dest_folder", 0)).trim();
		String bitRate = (settings.getElementByID("bit_rate", 0)).trim();
		String samplingRate = (settings.getElementByID("sampling_rate", 0)).trim();
			
		File src = new File(srcDirectory);
		File dest = new File (destDirectory);
		
		if(src.isDirectory()){
		if(!dest.exists()){
			dest.mkdir();
			}
		}
	File[] songs = src.listFiles();	
	for(File singleOne:songs){
		if(singleOne.isFile()){
		String temp=singleOne.toString();
		if(temp.endsWith("mp3")||temp.endsWith("MP3")){
			//String fileName = temp.substring(temp.lastIndexOf('\\')+1);
			String fileName = singleOne.getName();
			System.out.println(fileName);
			File target;
			if(srcDirectory.equals(destDirectory)){
				target = new File(dest.toString()+"\\temp"+fileName);//temporary file, later will be renamed
			}
			else{
				target = new File(dest.toString()+"\\"+fileName);
			}
				
			AudioFile af = AudioFileIO.read(singleOne);
			Tag originalTag = af.getTag();
			String title = originalTag.getFirst(FieldKey.TITLE);
			String artist = originalTag.getFirst(FieldKey.ARTIST);
			String album = originalTag.getFirst(FieldKey.ALBUM);
			AudioAttributes audio = new AudioAttributes();
			audio.setCodec("libmp3lame");
			audio.setBitRate(new Integer(bitRate));
			audio.setChannels(new Integer(2));
			audio.setSamplingRate(new Integer(samplingRate));
			EncodingAttributes attrs = new EncodingAttributes();
			attrs.setFormat("mp3");
			attrs.setAudioAttributes(audio);
			Encoder encoder = new Encoder();
			encoder.encode(singleOne, target, attrs);
			AudioFile afOutput = AudioFileIO.read(target);
			Tag output = afOutput.getTag();
			output.setField(FieldKey.TITLE, title);
			output.setField(FieldKey.ARTIST, artist);
			output.setField(FieldKey.ALBUM, album);
			try {
				afOutput.commit();
			} catch (CannotWriteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(srcDirectory.equals(destDirectory)){
				File temp2 = singleOne;
				singleOne.delete();
				target.renameTo(temp2);
			}
			
			}
		}
	}
	//long endTime=System.currentTimeMillis();
	//System.out.println("elspedTime"+(endTime-currentTime)/1000);
	}

}
